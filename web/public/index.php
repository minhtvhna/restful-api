<?php
/**
* 
*/
class Request
{
	public $url_elements;
	public $ver;
	public $parameter;

	function __construct()
	{
		$this->$ver = $_SERVER['REQUEST_METHOD'];
		$this->$url_elements = explode('/', $_SERVER['PATH_INFO']);
		//Goi ham parse o day thuc hien khi khoi tao doi tuong.
		$this->parseIncomingParams();
		//cai dat json la mac dinh.
		$this->format = 'json';
		if($this->parameters['format'])
		{
			$this->format = $this->parameters['format'];
		}
		//var_dump($this->$parameters);
		return true;
	}

	function parseIncomingParams()
	{
		$parameters = array();

        // Lay ve moi bien GET
        if (isset($_SERVER['QUERY_STRING'])) {
            parse_str($_SERVER['QUERY_STRING'], $parameters);
        }

        // Lay ve moi bien neu phuong thuc la POST hoac PULL.
        $body = file_get_contents("php://input");
        $content_type = false;
        if(isset($_SERVER['CONTENT_TYPE'])) {
            $content_type = $_SERVER['CONTENT_TYPE']; // kiem tra xem kieu cua conten la gi
        }
        // Phan nay lay ve noi dung trong content ma client gui len tuy thuoc vao header la gi.
        switch($content_type) {
            case "application/json":
                $body_params = json_decode($body);
                if($body_params) {
                    foreach($body_params as $param_name => $param_value) {
                        $parameters[$param_name] = $param_value;
                    }
                }
                $this->format = "json";
                break;
            case "application/x-www-form-urlencoded":
                parse_str($body, $postvars);
                foreach($postvars as $field => $value) {
                    $parameters[$field] = $value;

                }
                $this->format = "html";
                break;
            default:
                // part them o day nhung chua tim hiu het tam thoi the da.
                break;
        }
        $this->parameters = $parameters;
        //var_dump($this->$parameters);
	}

}
    $request = new Request;
    $controller_name = ucfirst($request->url_elements[1]) . 'Controller';
    var_dump($controller_name);
    echo($_SERVER['PATH_INFO']);
    //echo phpversion();
    //var_dump($request->parameters);
    var_dump($request->url_elements);
    //var_dump($request->ver);
?>